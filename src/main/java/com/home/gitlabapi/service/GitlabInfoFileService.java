package com.home.gitlabapi.service;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.home.gitlabapi.components.GitlabInfo;
import com.home.gitlabapi.dto.InfoFileDTO;
import com.home.gitlabapi.dto.InfoFileRepoInfoDTO;

@Service
public class GitlabInfoFileService {

	@Autowired
	private GitlabInfo gitlabInfo;
	
	@Autowired
	private GitlabRepoInfoService gitlabRepoInfoService;

	public InfoFileDTO getInfoFile(Long project_id)
			throws UnsupportedEncodingException, JsonMappingException, JsonProcessingException {

		// RestTemplate
		RestTemplate restTemplate = new RestTemplate();

		// Inclusao de Header
		HttpHeaders headers = new HttpHeaders();
		headers.add("user-agent", "API - GitLab");
		headers.add(gitlabInfo.getType(), gitlabInfo.getToken());

		StringBuilder builder = new StringBuilder();
		builder.append("https://gitlab.com/api/v4/projects/");
		builder.append(project_id);
		builder.append("/repository/files/");
		builder.append("src%2Fmain%2Fresources%2Fteste%2Eyaml/raw");
		builder.append("?ref=master");
		URI uri = URI.create(builder.toString());

		// Construindo a Request
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		// Faz a Chamada e Guarda a Resposta
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

		// Faz a Importacao e leitura do arquivo .yml
		Yaml yaml = new Yaml(new Constructor(InfoFileDTO.class));
		InfoFileDTO file = yaml.load(response.getBody().toString());

		// Completa o Objeto com dados do Repositorio
		InfoFileRepoInfoDTO resp = new InfoFileRepoInfoDTO();
		resp = gitlabRepoInfoService.repoInfo(project_id);

		List<InfoFileRepoInfoDTO> repoInfo = new ArrayList<InfoFileRepoInfoDTO>();
		repoInfo.add(resp);

		file.setRepoInfo(repoInfo);

		return file;
	}
}
