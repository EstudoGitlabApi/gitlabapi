package com.home.gitlabapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class GitlabapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabapiApplication.class, args);
	}


}
