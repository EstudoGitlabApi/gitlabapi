package com.home.gitlabapi.service;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.home.gitlabapi.components.GitlabInfo;
import com.home.gitlabapi.dto.InfoFileRepoInfoDTO;

@Service
public class GitlabRepoInfoService {
	
	@Autowired
	private GitlabInfo gitlabInfo;

	public InfoFileRepoInfoDTO repoInfo(Long project_id) throws JsonMappingException, JsonProcessingException {

		// RestTemplate
		RestTemplate restTemplate = new RestTemplate();

		// Inclusao de Header
		HttpHeaders headers = new HttpHeaders();
		headers.add("user-agent", "API - GitLab");
		headers.add(gitlabInfo.getType(), gitlabInfo.getToken());

		StringBuilder builder = new StringBuilder();
		builder.append("https://gitlab.com/api/v4/projects/");
		builder.append(project_id);
		URI uri = URI.create(builder.toString());

		// Construindo a Request
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		// Faz a Chamada e Guarda a Resposta
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

		String data_resp = response.getBody().toString();

		ObjectMapper mapper = new ObjectMapper();
		JsonNode json = mapper.readTree(data_resp);

		InfoFileRepoInfoDTO repoInfo = new InfoFileRepoInfoDTO();
		repoInfo.setName(json.path("name").asText());
		repoInfo.setPath_with_namespace(json.path("path_with_namespace").asText());
		repoInfo.setHttp_url_to_repo(json.path("http_url_to_repo").asText());

		return repoInfo;
	}

}
