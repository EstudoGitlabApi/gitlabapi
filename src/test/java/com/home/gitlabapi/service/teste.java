package com.home.gitlabapi.service;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.UnsupportedEncodingException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.home.gitlabapi.components.GitlabInfo;
import com.home.gitlabapi.dto.InfoFileDTO;
import com.home.gitlabapi.dto.InfoFileRepoInfoDTO;

@RunWith(MockitoJUnitRunner.class)
class teste {

	@Mock
    private RestTemplate restTemplate;
	
	@InjectMocks
	private GitlabInfoFileService gitlabInfoFileService;
	
	@Mock
	private GitlabRepoInfoService gitlabRepoInfoService;
	
	@Mock
	private GitlabInfo gitlabInfo;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void repoInfoOK() throws JsonMappingException, JsonProcessingException, UnsupportedEncodingException {
				
		GitlabInfo info = new GitlabInfo();
		info.setType("PRIVATE-KEY");
		info.setToken("z_u3A7zijbehvUw_n47J");
		
		Mockito.when(gitlabInfo.getType()).thenReturn(info.getType());
		Mockito.when(gitlabInfo.getToken()).thenReturn(info.getToken());
		
		File from = new File("teste.yaml");
		
		
		Mockito.doReturn((new ResponseEntity<String>(from.toString(), HttpStatus.OK))).when(restTemplate).exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any());
             
		
		InfoFileDTO resp = gitlabInfoFileService.getInfoFile(19979045L);
		
		InfoFileRepoInfoDTO repoInfo = new InfoFileRepoInfoDTO();
		repoInfo.setHttp_url_to_repo("");
		repoInfo.setName("");
		repoInfo.setPath_with_namespace("");
		
		Mockito.when(gitlabRepoInfoService.repoInfo(19979045L)).thenReturn(repoInfo);
		
		assertEquals("Henrique", resp.getFirstName().toString());
		
		
	}

}
