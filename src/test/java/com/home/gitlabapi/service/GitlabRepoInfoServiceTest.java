package com.home.gitlabapi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.File;
import java.io.UnsupportedEncodingException;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.home.gitlabapi.dto.InfoFileDTO;
import com.home.gitlabapi.dto.InfoFileRepoInfoDTO;

@RunWith(MockitoJUnitRunner.class)
class GitlabRepoInfoServiceTest {
	
	@InjectMocks
	private GitlabInfoFileService gitlabInfoFileService;
	
	@Mock
    private RestTemplate restTemplate;
	
	@Mock
	private GitlabRepoInfoService gitlabRepoInfoService;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void repoInfoOK() throws JsonMappingException, JsonProcessingException, UnsupportedEncodingException {
		
		File from = new File("teste.yaml");
		
		Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.eq(String.class)))
             .thenReturn(new ResponseEntity<String>(from.toString(), HttpStatus.OK));
		
		InfoFileRepoInfoDTO repoInfo = new InfoFileRepoInfoDTO();
		repoInfo.setHttp_url_to_repo("");
		repoInfo.setName("");
		repoInfo.setPath_with_namespace("");
		
		Mockito.when(gitlabRepoInfoService.repoInfo(1L)).thenReturn(repoInfo);
		
		InfoFileDTO resp = gitlabInfoFileService.getInfoFile(1L);
		
		assertEquals("Henrique", resp.getFirstName().toString());
		
		
	}

}
