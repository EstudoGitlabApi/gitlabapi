package com.home.gitlabapi.dto;

import java.util.List;

public class InfoFileDTO {

	private String firstName;
    private String lastName;
    private int age;
	private List<InfoFileRepoInfoDTO> repoInfo;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public List<InfoFileRepoInfoDTO> getRepoInfo() {
		return repoInfo;
	}
	public void setRepoInfo(List<InfoFileRepoInfoDTO> repoInfo) {
		this.repoInfo = repoInfo;
	}
}