package com.home.gitlabapi.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.home.gitlabapi.dto.InfoFileDTO;
import com.home.gitlabapi.service.GitlabInfoFileService;

@RestController
@RequestMapping("/gitlab/project/")
public class GitlabController {
	
	@Autowired
	GitlabInfoFileService gitlabInfoFileService;
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ResponseEntity<String> hello() {
		
		String resp = "API Ativa";
		
		return new ResponseEntity<String>(resp, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "{id}/infoFile", method = RequestMethod.GET)
	public ResponseEntity<?> infoFile(@PathVariable(name = "id") Long id) throws UnsupportedEncodingException, JSONException {
				
		try {
		
			InfoFileDTO resp = gitlabInfoFileService.getInfoFile(id);
			return new ResponseEntity<>(resp,HttpStatus.OK);
		
		}catch (Exception e){
			
				Map<String, String> responseBody = new HashMap<>();
				responseBody.put("error:", e.getMessage());
			
			return new ResponseEntity<Object>(responseBody, HttpStatus.BAD_REQUEST);
		}
		
	}

}
