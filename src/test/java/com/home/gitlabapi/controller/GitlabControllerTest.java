package com.home.gitlabapi.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import java.io.UnsupportedEncodingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.ResponseEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.home.gitlabapi.dto.InfoFileDTO;
import com.home.gitlabapi.service.GitlabInfoFileService;

@RunWith(MockitoJUnitRunner.class)
class GitlabControllerTest {
	
	@InjectMocks
	private GitlabController gitlabController;
	
	@Mock
	private GitlabInfoFileService gitlabInfoFileService;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void helloOk() {
		GitlabController gitlabController = new GitlabController();
		int result = gitlabController.hello().getStatusCodeValue();
		assertEquals(200, result);
	}
	
	@Test
	public void infoFileOk() throws UnsupportedEncodingException, JSONException, JsonMappingException, JsonProcessingException {
		
		InfoFileDTO obj = new InfoFileDTO();
		obj.setFirstName("Henrique");
		obj.setLastName("Repulho");
		obj.setAge(32);
		
		Mockito.when(gitlabInfoFileService.getInfoFile(1L)).thenReturn(obj);
		
		ResponseEntity<?> resp = gitlabController.infoFile(1L);
		
		verify(gitlabInfoFileService).getInfoFile(1L);
		
		assertEquals(200, resp.getStatusCodeValue());
		
	}
	
	@Test
	public void infoFileBadRequest() throws UnsupportedEncodingException, JSONException, JsonMappingException, JsonProcessingException {
		
		Mockito.when(gitlabInfoFileService.getInfoFile(1L)).thenThrow(JsonProcessingException.class);
		
		ResponseEntity<?> resp = gitlabController.infoFile(1L);
		
		verify(gitlabInfoFileService).getInfoFile(1L);
		
		assertEquals(400, resp.getStatusCodeValue());
		
	}
}
